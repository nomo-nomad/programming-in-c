#include <stdio.h>
#include <stdlib.h>

typedef struct {
    float width;
    float height;
} rect_t;

rect_t *Rect_init()
{
	rect_t *pNewRect = malloc(sizeof(rect_t));
	pNewRect->height = 0;
	pNewRect->width = 0;
	return pNewRect;
}

void Rect_width_set(rect_t *rect, float width)
{
	rect->width = width;
}

float Rect_width_get(rect_t *rect)
{
	return rect->width;
}

void Rect_height_set(rect_t *rect, float height)
{
	rect->height = height;
}
float Rect_height_get(rect_t *rect)
{
	return rect->height;
}

float Rect_area_get(rect_t *rect)
{
	return rect->height * rect->width;
}

void Rect_destroy(rect_t *rect)
{
	free(rect);
}

int
main()
{
	rect_t *rect = Rect_init();
	Rect_width_set(rect, 20.2f);
	Rect_height_set(rect, 14.3f);
	float ar = Rect_area_get(rect);
	printf("%.2f\n", ar);
}
