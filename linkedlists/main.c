#include <stdio.h>

int
main()
{
	int vals[] = { 19, 20, 0, 32, 41, 56, 91, 102, 43, 21 };
	int *nums = (int *)&vals;
	int i;
	for (i = 0; i < 6; i++)
	{
		/*this leads to overflow/out of bounds*/
		/*if (nums++ != NULL)*/
			nums++;
		printf("%d\n", *nums);
	}
	for (i = 0; i < 6; i++)
	{
		/*this leads to overflow/out of bounds*/
		if (nums[i] != 0)
			nums++;
		printf("%d\n", *nums);
	}
}
