#include <unistd.h>
#include <stdio.h>

int
main()
{
	/* Multi-Processing */
	int p1 = fork();

	/* p1 also executes this line and creates a "grandchild" */
	int p2 = fork();

	if (p1 > 0 && p2 > 0)
	{
		printf("Parent\n");
		printf("%d, %d \n", p1, p2);
		printf("My pid is: %d \n", getpid());
	} 
	else if (p1 == 0 && p2 > 0)
	{
		printf("First child\n");
		printf("%d, %d \n", p1, p2);
		printf("My pid is: %d \n", getpid());
	}
	else if (p1 > 0 && p2 == 0)
	{
		printf("Second child\n");
		printf("%d, %d \n", p1, p2);
		printf("My pid is: %d \n", getpid());
	}
	else
	{
		printf("Third child\n");
		printf("%d, %d \n", p1, p2);
		printf("My pid is: %d \n", getpid());
	}
	return 0;
}
