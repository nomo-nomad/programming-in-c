# Programming in C

Miscellaneous things I throw together while becoming familiar with C and
lower-level programming concepts.

## Topics or Areas of Interest

- Properly defining and including headers
- Building my own custom data-types
- Linked Lists
- r and l values

## Objects and Data Types

Hide your internal structure implementation behind a void pointer or pointer to
a struct?.

Math and computation tends to be almost free (it's a computer, that's what it
does).

For Example:

```c
/* You've technically stored area twice, because with the length and width
* you can compute the area almost instantaneously. */
typedef struct Rect {
    float width;
    float height;
    float area;
} rect_t;

/* You also have a potential bug waiting to appear because area itself can be
* 'lagging' behind length and width if it's not recomputed upon changing
* length or width variables */
```

```c
typedef struct Rect {
    float width;
    float height;
} rect_t;

void Rect_width_set(rect_t *rect, float width);
float Rect_width_get(rect_t *rect);
void Rect_length_set(rect_t *rect, float length);
float Rect_length_get(rect_t *rect);
float Rect_length_get(rect_t *rect);

```

## Types of Graphs

### Undirected Graphs

edges have no orientation. The edge (u,v) is identical to the edge (v, u)

### Directed Graphs

Edges have orientations. The edge (u, v) is the edge _from_ node u to node
v.

### Weighted Graphs

Many graphs can contain a certain weight to represent an arbirtrary
value such as cost, distance, quantity, etc...

### Trees

A tree could be considered an undirected graph with no cycles

### Rooted Trees

Rooted trees are trees with a _designated root node_ where every edge points
away from or towards the root node. When edges point away from the root the
graph is called an arborescence (out-tree) and anti-arborescence (in-tree)
otherwise.

### Directed Acyclic Graphs (DAGs)

Directed graphs with no cycles. These graphs play an important role in
representing structures with dependencies. Several efficient algos exist to
operate on DAGs.

Cool fact: All out-trees are DAGs, but not all DAGs are out-trees.

Steenberg, Eskil. How I Program C. Invidious, 21 Nov. 2016,
invidio.us/watch?v=443UNeGrFoM.
