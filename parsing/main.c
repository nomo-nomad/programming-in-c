#include <stdio.h>
#include <stdlib.h>

#define ALLOCSIZE 2048
#define LINESIZE 50

static char allocbuf[ALLOCSIZE];
static char *allocp = allocbuf;

char*
alloc(int n)
{
  if (allocbuf + ALLOCSIZE - allocp >= n) {
    allocp += n;
    return allocp - n;
  } else
    return 0;
}

void
afree(char *p)
{
  if (p >= allocbuf && p < allocbuf + ALLOCSIZE)
    allocp = p;
}

static void
bw_strcpy(char *src, char *dest)
{
  while ((*dest++ = *src++))
    ;
}

int main(int argc, char **argv)
{
  FILE *orders = NULL;
  char word[128];
  char **words = calloc(3, sizeof *words);
  int c, i, j;

  orders = fopen("BTCFills.csv", "r");

  if (orders)
  {
    for (i = 0, j = 0; (c = fgetc(orders)) != '\n'; i++)
    {
      if (c == ',') {
        word[i] = '\0';
        if ((words[j] = calloc(i, sizeof *word))) {
          bw_strcpy(word, words[j]);
          j += 1;
          i = 0;
        }
      }
      word[i] = c;
    }

    printf("%s\n", words[0]);
    printf("%s\n", words[1]);
    printf("%s\n", word);
  }

  return 0;
}
